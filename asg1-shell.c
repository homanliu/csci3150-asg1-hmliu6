/*
* CSCI3150 Assignment 2 - Writing a Simple Shell
* Feel free to modify the given code.
* Press Ctrl-D to end the program
*
*  You may put down your reference here:
*
*/

/* Header Declaration */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <stdbool.h>
#include <sys/wait.h>

char *stack[100] = {NULL};
int top = -1;

/* Function Declaration */
int getUserInput(char* input);
void tokenizeInput(char* input);
void signalHandler(int signal);
bool commandExecute(char* input[]);
int specialCommand(char* command[]);

/* Functions */
void signalHandler(int signal){
  return;
}

int main(int argc,char* argv[]){
  int isExit = 0;
  signal(SIGINT, signalHandler);
  signal(SIGTERM, signalHandler);
  signal(SIGQUIT, signalHandler);
  signal(SIGTSTP, signalHandler);
  do{
    char rawInput[255];
    isExit = getUserInput(rawInput);
    if(isExit) break;

    tokenizeInput(rawInput);


  }while(isExit == 0);
  return 0;
}

/*
  GetUserInput()
  - To parse User Input and remove new line character at the end.
  - Copy the cleansed input to parameter.
  - Return 1 if encountered EOF (Ctrl-D), 0 otherwise.
*/
int getUserInput(char* input){
  char buf[255], cwd[1024];
  char *s = buf;
  getcwd(cwd, sizeof(cwd));
  printf("[3150 Shell:%s]=> ",cwd);
  if(fgets(buf,255,stdin) == NULL){
    putchar('\n');
    return 1;
  }
  // Remove \n
  for(;*s != '\n';s++);
  *s = '\0';

  if(strcmp(buf, "bye") == 0){
    return 1;
  }
  strcpy(input,buf);
  return 0;
}
/*
  tokenizeInput()
  - Tokenize the string stored in parameter, delimiter is space
  - In given code, it only prints out the token.
  Please implement the remaining part.
*/

void tokenizeInput(char* input){
  char buf[255];
  char* arguments[10] = {NULL};
  int count = 0, i;
  bool runCommand = true, notFound = false;
  strcpy(buf,input);
  char* token = strtok(buf," ");

  while(token != NULL){
    if(strcmp(token, "&&") && strcmp(token, "||")){
      arguments[count] = malloc(255 * sizeof(char));
      strcpy(arguments[count], token);
      count += 1;
    }
    else if(strcmp(token, "&&") == 0){
      runCommand = commandExecute(arguments);
      if(runCommand == false){
        notFound = true;
        break;
      }
      else{
        for(i = 0; i < 10; i++)
          arguments[i] = NULL;
        count = 0;
      }
    }
    else if(strcmp(token, "||") == 0){
      runCommand = commandExecute(arguments);
      if(runCommand == true){
        notFound = true;
        break;
      }
      for(i = 0; i < 10; i++)
        arguments[i] = NULL;
      count = 0;
    }
    token = strtok(NULL," ");
  }
  if(!notFound)
    commandExecute(arguments);
  return;
}

bool commandExecute(char* input[]){
  int pid, i, j, exitcode;
  char source[3][50] = {"/bin/", "/usr/bin/", "./"};
  char command[100];
  int checkSpecial = 0;
  checkSpecial = specialCommand(input);
  if(checkSpecial == 1){
    return true;
  }
  else if(checkSpecial == 0){
    strcpy(command, input[0]);
    pid = fork();
    if(pid == 0){
      for(i=0; i<3; i++){
        strcat(source[i], command);
        strcpy(input[0], source[i]);
        execv(input[0], input);
      }
      fprintf(stdout, "{%s}: command not found\n", command);
  		exit(-1);
    }
    else{
      wait(&exitcode);
      if(exitcode == 0)
        return true;
    }
  }
  return false;
}

int specialCommand(char* command[]){
  const char *specficCommands[] = {"gofolder", "push", "pop", "dirs", "bye"};
  const int specificArgs[5] = {2, 2, 1, 1, 1};
  char buffer[255];
  int i, j;
  for(i = 0; i < 5; i++){
    if(strcmp(command[0], specficCommands[i]) == 0){
      for(j = 1; j < 10; j++)
        if(command[j] == NULL)
          break;
      if(specificArgs[i] != j){
        printf("%s: wrong number of arguments\n", command[0]);
        return -1;
      }
      // Correct number of arguments and specfic command
      else{
        if(strcmp(command[0], "bye") == 0)
          exit(-1);
        else if(strcmp(command[0], "dirs") == 0){
          for(int i = top; i > -1; i--)
            printf("%d %s\n", top - i, stack[i]);
        }
        else if(strcmp(command[0], "pop") == 0){
          if(top == -1)
            printf("pop: directory stack empty\n");
          else{
            chdir(stack[top]);
            top -= 1;
            for(int i = top; i > -1; i--)
              printf("%d %s\n", top - i, stack[i]);
          }
        }
        else if(strcmp(command[0], "push") == 0){
          top += 1;
          getcwd(buffer, sizeof(buffer));
          stack[top] = malloc(sizeof(buffer));
          strcpy(stack[top], buffer);
          chdir(command[1]);
          for(int i = top; i > -1; i--)
            printf("%d %s\n", top - i, stack[i]);
        }
        else if(strcmp(command[0], "gofolder") == 0){
          int ret;
          ret = chdir(command[1]);
          if(ret == -1)
            printf("{%s}: cannot change directory\n", command[1]);
        }
      }
      return 1;
    }
  }
  return 0;
}
